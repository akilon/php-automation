<?php 

class Wealthx {
	
	protected $remoteDir, $localDir, $dbName;

    public function __construct() {
        global $remoteDir, $localDir, $dbName;
        $this->remoteDir =& $remoteDir;
        $this->localDir =& $localDir;
        $this->dbName =& $dbName;
    }

	public function connectDB (){		
		$config = new Config();
		$db = new Zebra_Database();
		$db->connect($config->db_host, $config->db_user, $config->db_pass, $config->db, $config->db_port);
		$db->debug = false;
		return $db;
	}

	function sendEmail($emailBody){

		$body = implode("<br>", $emailBody);

		$email = new PHPMailer();
		$email->From      = 'FactsetDaily@wealthx.com';
		$email->FromName  = 'Factset Daily Updates';
		$email->Subject   = 'Factset Daily Updates for '. date("jS F");
		$email->Body      = $body;
		$email->AddAddress( 'akrishnan@wealthx.com' );
		echo "Email sent" . PHP_EOL;
		return $email->Send();
	}

	public function postBatch(){
		$db = $this->connectDB();
		$db->query('UPDATE fact_iso_currency SET ACTIVE=0 WHERE ISO_CURRENCY=\'CNH\'');
	}


	public function connectFTP(){
		$config = new Config();

		if (!function_exists("ssh2_connect"))
		  die('Function ssh2_connect not found, you cannot use ssh2 here');

		if (!$connection = ssh2_connect($config->ftp_host, $config->ftp_port))
		  die('Unable to connect');

		if (!ssh2_auth_password($connection, $config->ftp_user, $config->ftp_pass))
		  die('Unable to authenticate.');

		if (!$stream = ssh2_sftp($connection))
		  die('Unable to create a stream.');

		if (!$dir = opendir("ssh2.sftp://{$stream}{$this->remoteDir}"))
		  die('Could not open the directory');

		if ($this->remoteDir == '')
		  die('remoteDir cannot be blank.');

		if ($this->localDir == '')
		  die('localDir cannot be blank.');

		$ftp = array();
		$ftp['dir'] = $dir;
		$ftp['stream'] = $stream;

		return $ftp;
	}

	public function getDelta($fn, $fn_x){

		$db = $this->connectDB();
		$ftp = $this->connectFTP();

		//grab all existing filenames in db and put in array for 
		//comparison to find out files that is not in db
		$files_db = array();
		$db->query('SELECT filename FROM factdaily_ftpfiles WHERE localDir = ?', array($this->localDir));
		while ($row = $db->fetch_assoc()) {
			$files_db[] = $row["filename"];
		}
		//print_r($files_db);

		$files = array();

		// browse all files in remote dir and loop each file found.
		while (false !== ($file = readdir($ftp['dir']))) {
			if ($file == "." || $file == "..")
			  	continue;

			  //check if match with file name provided
			if (fnmatch($fn, $file)) {
				
				if ($fn_x){
					//this is just for safety net.. if match with fn_x then skip 
					//to next file. If not required, just leave it blank.
					if (fnmatch($fn_x, $file))
						continue;
				}
			  
			  //get filesize and created time
			  $statinfo = ssh2_sftp_stat($ftp['stream'], $this->remoteDir.$file);
			  echo "Found files on remote : " . $this->remoteDir.$file . PHP_EOL;
			  $filesize = $statinfo['size'];
			  $atime = date("Y-m-d H:i:s",$statinfo['atime']);

			  //check if file exist in db
			  if (!in_array($file, $files_db)) {
			    //new file must register to database and add to the $files array for processing
			    $db->query('INSERT factdaily_ftpfiles SET filename = ?, filesize = ?, createdon = ?, localDir = ?, remoteDir = ? ', array($file, $filesize, $atime, $this->localDir, $this->remoteDir));
			    $files[]['filename'] = $file;
			  }

			}
		}
		$this->downloadNewFiles($files);
	}

	//need to pass in array in the format of $file['filename'];
	public function downloadNewFiles($files = null){

		$db = $this->connectDB();
		$ftp = $this->connectFTP();

		$stream = $ftp['stream'];
		$remoteDir = $this->remoteDir;
		$localDir = $this->localDir;
		$unpack = array();

		$emailBody = array();

		$total = count($files);

		//$this->sendEmail($emailBody);

		if ($total > 0){
			echo "Found $total files to be uploaded in $remoteDir" . PHP_EOL;
			$emailBody[] = "Found $total files to be uploaded in $remoteDir";

			foreach($files as $item) {
				$file = $item['filename'];
				$status = "Done";				

				if (!$remote = @fopen("ssh2.sftp://{$stream}/{$remoteDir}{$file}", 'r')){
					$status = "Unable to open remote file: $file";
				}

				if (!$local = @fopen($localDir . $file, 'w')){
					$status = "Unable to create local file: $file";
				}

				$read = 0;
				$filesize = filesize("ssh2.sftp://{$stream}/{$remoteDir}{$file}");
				while ($read < $filesize && ($buffer = fread($remote, $filesize - $read))){
					$read += strlen($buffer);
					if (fwrite($local, $buffer) === FALSE){
						$status = "Unable to write to local file: $file";
					}
				}

				$db->query('UPDATE factdaily_ftpfiles SET download_status = ? WHERE filename = ? AND localDir = ?', array($status, $file, $localDir));
				
				echo "Done upload for $file with status : ".$status . PHP_EOL;

				fclose($local);
				fclose($remote);

				if ($status == "Done"){
					$file_split = explode(".", $file);
					if ($file_split[1] == "txt") {
						$db->query('UPDATE factdaily_ftpfiles SET unpack_files = ?, unpack_dir = ? WHERE filename = ? AND localDir = ?', array($file, $localDir, $file, $localDir));
					} else {
						$unpack[] = $file;	
					}
								
				}

				$emailBody[] = $file . " - " . $status;
			}

			$this->sendEmail($emailBody);
		}

		//Run unpack 
		if (count($unpack) > 0) {
			foreach ($unpack as $file) {
				echo "Unpacking ".$file . PHP_EOL;

				$unpack_dir = explode(".", $file);
				$unpack_dir = $localDir.$unpack_dir[0].'/';
				if (!file_exists($unpack_dir)) {
				mkdir($unpack_dir, 0777, true);
				}
				chmod($localDir.$file, 0777);
				chmod($unpack_dir, 0777);

				//echo $unpack_dir.$file;

				$zipfiles = array();
				$zip = new ZipArchive;
				$res = $zip->open($localDir.$file);

				if ($res === TRUE) {
					$zip->extractTo($unpack_dir); 
					echo "Unpacking ".$file . " completed" . PHP_EOL;

					for ($i = 0; $i < $zip->numFiles; $i++) {
	                  $filename =  $zip->getNameIndex($i);
	                  $zipfiles[] = $filename;
	                  chmod($unpack_dir.$filename, 0777);
	                }

                $zipfiles_str = implode(",", $zipfiles);
                
                $zip->close();

                unlink($localDir.$file);

                $db->query('UPDATE factdaily_ftpfiles SET unpack_status = \'Done\', unpack_files = ?, unpack_dir = ? WHERE filename = ?', array($zipfiles_str, $unpack_dir, $file));
              } else {
                $db->query('UPDATE factdaily_ftpfiles SET unpack_status = \'Failed\' WHERE filename = ?', array($file));
              }

			}
		}

	}


	public function updateDBDelta($fnupdate, $fndelete = null) {

		$db = $this->connectDB();

		$files_db = array();
		$db->query('SELECT unpack_files, unpack_dir, filename FROM factdaily_ftpfiles WHERE localDir = ? AND updatetodb_status IS NULL', array($this->localDir));
		$items = $db->fetch_assoc_all();
		$total = count($items);

		$error = 0;
		echo "Found $total files to be updated into " . $this->dbName . "." . PHP_EOL;

		if ($total > 0 ) {
			foreach($items as $key => $value) {
				$unpack_files = $value['unpack_files'];
				$unpack_dir = $value['unpack_dir'];
				$filename_db = $value['filename'];

				$files = explode(",", $unpack_files);

				$errorMsg = '';

				foreach($files as $filename) {

					$updatecount = 0;					

					if (isset($fnupdate) and fnmatch($fnupdate, $filename)) {
						$handle = fopen($unpack_dir.$filename, "r");
						if ($handle) {
						    $b = 0;
						    while (($line = fgets($handle)) !== false) {
						        // process the line read.

						        $update = array();
						        //echo "strlen : '" . $line . "'<br>";
						        if (strlen($line) > 2){
						          if ($b == 0) {
						            $labels = $line;
						            $labels = explode("|", $labels);
						            $labels_str = implode(",", $labels);
						            $labels_str = str_replace('"', '', $labels_str);
						            $labels_str = str_replace('/', '_', $labels_str);
						            $labels_str = str_replace(' ', '_', $labels_str);
						            //echo $labels_str . PHP_EOL;
						          } else {
						            $line = str_replace('"', '', $line);
						            $line = explode("|", $line);  

						            $totalcol = count($line);
						            for ($a = 0; $a < $totalcol; $a++) {

						            	$labels_filter = str_replace('/', '_', $labels[$a]);
										$labels_filter = str_replace(' ', '_', $labels_filter);
										$labels_filter = str_replace('"', '', $labels_filter);

						              $update[] = $labels_filter . ' = "' . $line[$a] . '"';
						            }

						            	$queryStr = 'INSERT INTO '.$this->dbName.' ('.$labels_str.') VALUES ("' . implode('", "', $line) . '") ON DUPLICATE KEY UPDATE ' . implode(', ', $update);
									    $try = $db->query($queryStr);
							            if (!$try){
							            	$errorMsg .= $queryStr. "\n";							            	
							            }
							            $updatecount++;
							           
						          }
						        }

						        $b++;                            
						    }

						    fclose($handle);
						} else {
						    // error opening the file.
						} 
					}


					if (isset($fndelete) and fnmatch($fndelete, $filename)) {
						$handle = fopen($unpack_dir.$filename, "r");
						if ($handle) {
						    $b = 0;

						    $db->query('SHOW KEYS FROM '.$this->dbName.' WHERE Key_name = \'PRIMARY\'');
						    $keys = $db->fetch_assoc_all();
						    echo json_encode($keys) . PHP_EOL;
						    
						    while (($line = fgets($handle)) !== false) {
						        // process the line read.

						        $delete = array();
						        //echo "strlen : '" . $line . "'<br>";
						        if (strlen($line) > 2) {
						          if ($b == 0) {
						            $labels = $line;
						            $labels = explode("|", $labels);
						            $labels = str_replace('"', '', $labels);
						            $labels_str = implode(",", $labels);    
						            $labels_str = str_replace('/', '_', $labels_str);                            
						            //echo $labels_str;
						          } else {
						            $line = str_replace('"', '', $line);
						            $lines = explode("|", $line);  

						            $totalcol = count($lines);
						            for ($a = 0; $a < $totalcol; $a++) {
						              foreach ($keys as $key => $val) {
						                if ($val["Column_name"] == $labels[$a]){
						                  $delete[] = $labels[$a] . ' = "' . $lines[$a] . '"';
						                  
						                }
						                
						              }
						            }

						            	$queryStr = 'DELETE FROM '.$this->dbName.' WHERE ' . implode(' AND ', $delete);
									    $try = $db->query($queryStr);
									    echo "Removed line " . $updatecount . PHP_EOL;
							            if (!$try){
							            	$errorMsg .= $queryStr. "\n";
							            	
							            }

						          }
						        }

						        $b++;                            
						    }

						    fclose($handle);
						} else {
						    // error opening the file.
						} 

					}

				}


				if 	($error == 0) {
					if ($errorMsg == '') {
						$db->query('UPDATE factdaily_ftpfiles SET updatetodb_status = \'Done\' WHERE filename = ?', array($filename_db));
						echo "Updated db status without error : " . $filename_db . PHP_EOL;
					} else {
						$db->query('UPDATE factdaily_ftpfiles SET updatetodb_status = \'Failed\', errorMsg = ? WHERE filename = ?', array($filename_db, $errorMsg));
						echo "Updated db status with error : " . $filename_db . PHP_EOL;
						$error = 1;
					}					
				}

				
				$this->deleteDir($unpack_dir);
			}
		}


	}

	public static function deleteDir($dirPath) {
	    if (! is_dir($dirPath)) {
	        throw new InvalidArgumentException("$dirPath must be a directory");
	    }
	    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
	        $dirPath .= '/';
	    }
	    $files = glob($dirPath . '*', GLOB_MARK);
	    foreach ($files as $file) {
	        if (is_dir($file)) {
	            self::deleteDir($file);
	            rmdir($file);
	        } else {
	            unlink($file);
	        }
	    }
	    //rmdir($dirPath);
	}

}
?>




















