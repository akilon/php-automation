<?php
  require 'lib/zebradb/Zebra_Database.php';   

  require 'class/MysqlConnection.php';
  require 'class/AppController.php';
  require 'class/DS_Dao.php';
  require 'class/DS_Controller.php';

  $occ = new DS_Controller();

  $occ->dailyStockOption();
  $occ->dailyCommonStock();
  $occ->dailyPrivateCompany();
  $occ->dailyAsset();
  $occ->calculateTotalNetWorth();
?>