<?php

class DS_Dao {

    function __construct(){
        $this->db = new MysqlConnection();
    }

    public function dailyStockOption_query(){
		$db = $this->db->connectDB();
		//$db_occ = $this->connectDB_occ();
		$db->query("
        SELECT 
            a.includeInAssetSwatch,
            a.manualSharePrice,
            a.profileID,
            a.valueOfHolding,
            a.actualValueofHolding,
            a.roundedValueofHolding,
            a.ID,
            a.numberofUnits,
            a.strikePrice,
            a.currency,
            a.tickerSymbolID,
            a.exercisable,
            a.fxRate,
            a.marketPrice,
            a.priceDate,
            c.fs_perm_sec_id,
            d.p_price,
			d.date AS p_price_date,
            e.exch_rate_usd,
            e.iso_currency
        FROM
            networthpublicholdingsstockoptions a
                LEFT JOIN
            profilebasic b ON b.ID = a.profileID
                LEFT JOIN
            fact_security_ticker c ON c.ticker_exchange = a.tickerSymbolID
                LEFT JOIN
            fp_basic_bd d ON c.fs_perm_sec_id = d.fs_perm_sec_id AND DATE = CURDATE()-1
                LEFT JOIN
            (SELECT exch_rate_usd, iso_currency, date FROM fx_rates_usd WHERE date = CURDATE()-1 GROUP BY iso_currency) e ON e.iso_currency = a.currency
                LEFT JOIN
            networthsummary f ON f.profileID = a.profileID
        WHERE
            (a.tickerSymbolID != '' OR a.tickerSymbolID IS NOT NULL)
                AND a.numberofUnits > 0
                AND a.manualSharePrice = 0
                AND f.estimatedNetWorthLowerActual > 0
                AND e.iso_currency IS NOT NULL
                AND b.dossierState != 'deceased'
                AND NOT EXISTS( SELECT * FROM factdaily_occdailylog WHERE recordID = a.ID AND runType = 'networthpublicholdingsstockoptions' AND createdDate = CURDATE())
                AND d.p_price IS NOT NULL
        ");
		$items = $db->fetch_assoc_all();   
		$db->close();	

		return $items;
	}

	public function dailyCommonStock_query(){
		$db = $this->db->connectDB();
		//$db_occ = $this->connectDB_occ();
		$db->query("
        SELECT 
            a.includeInAssetSwatch,
			a.manualSharePrice,
			a.profileID,
			a.valueOfHolding,
			a.roundedValueofHolding,
			a.actualValueofHolding,
			a.ID,
			a.numberofUnits,
			a.currency,
			a.tickerSymbolID,
			a.fxRate,
			a.marketPrice,
			a.priceDate,
            c.fs_perm_sec_id,
            d.p_price,
			d.date AS p_price_date,
            e.exch_rate_usd,
            e.iso_currency
        FROM
            networthpublicholdingscommonstock a
                LEFT JOIN
            profilebasic b ON b.ID = a.profileID
                LEFT JOIN
            fact_security_ticker c ON c.ticker_exchange = a.tickerSymbolID
                LEFT JOIN
            fp_basic_bd d ON c.fs_perm_sec_id = d.fs_perm_sec_id AND DATE = CURDATE()-1
                LEFT JOIN
            (SELECT exch_rate_usd, iso_currency, date FROM fx_rates_usd WHERE date = CURDATE()-1 GROUP BY iso_currency) e ON e.iso_currency = a.currency
                LEFT JOIN
            networthsummary f ON f.profileID = a.profileID
        WHERE
            (a.tickerSymbolID != '' OR a.tickerSymbolID IS NOT NULL)
                AND a.numberofUnits > 0
                AND a.manualSharePrice = 0
                AND f.estimatedNetWorthLowerActual > 0
                AND e.iso_currency IS NOT NULL
                AND b.dossierState != 'deceased'
                AND NOT EXISTS(SELECT * FROM factdaily_occdailylog WHERE recordID = a.ID AND runType = 'networthpublicholdingscommonstock' AND createdDate = CURDATE())
                AND d.p_price IS NOT NULL
        ");
		$items = $db->fetch_assoc_all();   
		$db->close();	

		return $items;
	}

	public function dailyPrivateCompany_query(){
		$db = $this->db->connectDB();
		$db->query("
        SELECT 
            a.ID, 
            a.profileID, 
            a.currency,  
            a.acctualValueofHoldingLocal, 
            a.actualValueofHoldingUSD, 
            a.roundedValueofHoldingUSD, 
            a.currencyValue, 
            a.psRatioManual,
            e.exch_rate_usd,
            e.iso_currency as currency
        FROM
            networthprivateholdings a
                LEFT JOIN
            profilebasic b ON b.ID = a.profileID
                LEFT JOIN
            (SELECT exch_rate_usd, iso_currency, date FROM fx_rates_usd WHERE date = CURDATE()-1 GROUP BY iso_currency) e ON e.iso_currency = a.currency
                LEFT JOIN
            networthsummary c ON c.profileID = a.profileID
        WHERE
            b.dossierState != 'deceased'
            AND a.psRatioManual = 0
            AND c.estimatedNetWorthLowerActual > 0
            AND e.iso_currency IS NOT NULL
            AND NOT EXISTS(SELECT * FROM factdaily_occdailylog WHERE recordID = a.ID AND runType = 'networthprivateholdings' AND createdDate = CURDATE())
        ");
		$items = $db->fetch_assoc_all();   
		$db->close();	

		return $items;
	}

	public function dailyAsset_query(){
		$db = $this->db->connectDB();
		$db->query("
        SELECT 
            a.ID,
            a.profileID,
            a.currencyFrom,
            a.valueOfHolding,
            a.valueofHoldingLocalCurrency,
            a.valueofHoldingForeign,
            a.roundedValueofHolding,
            a.currencyRate,
            a.currencyFrom,
            e.exch_rate_usd,
            e.iso_currency as currency
        FROM
            networthalternativeassets a
                LEFT JOIN
            profilebasic b ON b.ID = a.profileID
                LEFT JOIN
            (SELECT exch_rate_usd, iso_currency, date FROM fx_rates_usd WHERE date = CURDATE()-1 GROUP BY iso_currency) e ON e.iso_currency = a.currencyFrom
                LEFT JOIN
            networthsummary c ON c.profileID = a.profileID
        WHERE
            a.currencyFrom IS NOT NULL
            AND b.dossierState != 'deceased'
            AND c.estimatedNetWorthLowerActual > 0
            AND e.iso_currency IS NOT NULL
            AND NOT EXISTS(SELECT * FROM factdaily_occdailylog WHERE recordID = a.ID AND runType = 'networthalternativeassets' AND createdDate = CURDATE())
        ");
		$items = $db->fetch_assoc_all();   
		$db->close();	

		return $items;
	}
}
?>