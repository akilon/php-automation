<?php
class DS_Controller extends AppController{

    function __construct(){
		$this->db = new MysqlConnection();
		$this->dao = new DS_Dao();
	}

    public function dailyStockOption () {
		$items = $this->dao->dailyStockOption_query();
		$logs = array();
		foreach ($items as $item) {

			//CHANGE THIS
            $item["db"] = "networthpublicholdingsstockoptions";

			//POPULATING FIELDS
            $fxRate = ($item['currency'] == "USD") ? 1 : $item['exch_rate_usd'];
			$fxRate = number_format($fxRate, 5, '.', '');

			$item['numberofUnits_wsplit'] = $this->stockSplit(
                $item['numberofUnits'], 
                $item['fs_perm_sec_id'], 
                $item['priceDate'], 
                $item['db'], 
                $item['ID']);

            //CHECK FOR STOCKSPLIT AND EXERSISABLE
            $actualValue = $this->exercisable($item);
			$actualValue = $actualValue * $fxRate;
			$roundValue = $this->roundingRules($actualValue);
			$roundValue = floor($roundValue);
			$actualValue = number_format($actualValue, 2, '.', '');

			//IF ROUNDVALUE IS ZERO THEN ASSETSWATCH SHOULD BE FALSE
			$includeInAssetSwatch = ($roundValue > 0) ? 'true' : 'false' ;

            //CHANGE THIS - FIELDS THAT WILL BE UPDATED
			$columns = array(
                "valueOfHolding",
                "actualValueofHolding", 
                "roundedValueofHolding", 
                "numberofUnits", 
                "fxRate", 
                "marketPrice", 
                "priceDate", 
                "includeInAssetSwatch");

			$newValue = array(
				"valueOfHolding" => $roundValue,
				"actualValueofHolding" => $actualValue,
				"roundedValueofHolding" => $roundValue,
				"numberofUnits" => $item['numberofUnits_wsplit'],
				"fxRate" => $fxRate,
				"marketPrice" => $item['p_price'],
				"priceDate" => $item['p_price_date'],
				"includeInAssetSwatch" => $includeInAssetSwatch
				);

			$query = $this->getQuery($item, $newValue, $columns);
			
			//$logs[] = "DSO>> after =>". $query['after'] . " before =>". $query['before'] . " ID=> ". $item['ID'] . " currency=> " . $item['currency'] . PHP_EOL;
			$item['before'] = $query['before'];
			$item['after'] = $query['after'];
			$logs[] = $item;
			$bulkUpdate[] = "UPDATE networthpublicholdingsstockoptions SET " . $query['after'] . "updatedDate = NOW(), updaterID = '1703' WHERE ID = " . $item['ID'];

		} //foreach

		if (isset($bulkUpdate)) $updatedb = $this->runBulkUpdate($bulkUpdate);
		if (isset($logs))$log = $this->writeLog($logs);
	}


	public function dailyCommonStock () {
		$items = $this->dao->dailyStockOption_query();
		$logs = array();
		foreach ($items as $item) {

			//CHANGE THIS
            $item["db"] = "networthpublicholdingscommonstock";

			//POPULATING FIELDS
            $fxRate = ($item['currency'] == "USD") ? 1 : $item['exch_rate_usd'];
			$fxRate = number_format($fxRate, 5, '.', '');

			$item['numberofUnits_wsplit'] = $this->stockSplit(
                $item['numberofUnits'], 
                $item['fs_perm_sec_id'], 
                $item['priceDate'], 
                $item['db'], 
                $item['ID']);

            //CHECK FOR STOCKSPLIT AND EXERSISABLE

			$actualValue = ($item['p_price'] * $item['numberofUnits_wsplit']) * $fxRate;
			$roundValue = $this->roundingRules($actualValue);
			$roundValue = floor($roundValue);
			$actualValue = number_format($actualValue, 2, '.', '');

			$columns = array(
				"valueOfHolding", 
				"actualValueofHolding", 
				"roundedValueofHolding", 
				"numberofUnits", 
				"fxRate", 
				"marketPrice", 
				"priceDate"
				);

			$newValue = array(
				"valueOfHolding" => $roundValue,
				"actualValueofHolding" => $actualValue,
				"roundedValueofHolding" => $roundValue,
				"numberofUnits" => $item['numberofUnits_wsplit'],
				"fxRate" => $fxRate,
				"marketPrice" => $item['p_price'],
				"priceDate" => $item['p_price_date']
				);	

			$query = $this->getQuery($item, $newValue, $columns);
			
			//$logs[] = "DSO>> after =>". $query['after'] . " before =>". $query['before'] . " ID=> ". $item['ID'] . " currency=> " . $item['currency'] . PHP_EOL;
			$item['before'] = $query['before'];
			$item['after'] = $query['after'];
			$logs[] = $item;
			$bulkUpdate[] = "UPDATE networthpublicholdingscommonstock SET " . $query['after'] . "updatedDate = NOW(), updaterID = '1703' WHERE ID = " . $item['ID'];
		}

		if (isset($bulkUpdate)) $updatedb = $this->runBulkUpdate($bulkUpdate);
		if (isset($logs))$log = $this->writeLog($logs);
	} 


	public function dailyPrivateCompany(){
		$items = $this->dao->dailyPrivateCompany_query();
		$logs = array();
		foreach ($items as $item) {

			$item["db"] = "networthprivateholdings";
			$item["tickerSymbolID"] = '';

			//POPULATING FIELDS
            $fxRate = ($item['currency'] == "USD") ? 1 : $item['exch_rate_usd'];
			$fxRate = number_format($fxRate, 5, '.', '');

			$actualValueofHoldingUSD = $item['acctualValueofHoldingLocal'] * $item['currencyValue'];
			$roundedValueofHoldingUSD = $this->roundingRules($actualValueofHoldingUSD);

			$columns = array(
				"actualValueofHoldingUSD", 
				"roundedValueofHoldingUSD", 
				"currencyValue"
				);

			$newValue = array(
				"actualValueofHoldingUSD" => $actualValueofHoldingUSD,
				"roundedValueofHoldingUSD" => $roundedValueofHoldingUSD,
				"currencyValue" => $item['currencyValue']
				);

			$query = $this->getQuery($item, $newValue, $columns);
			
			//$logs[] = "DSO>> after =>". $query['after'] . " before =>". $query['before'] . " ID=> ". $item['ID'] . " currency=> " . $item['currency'] . PHP_EOL;
			$item['before'] = $query['before'];
			$item['after'] = $query['after'];
			$logs[] = $item;
			$bulkUpdate[] = "UPDATE networthprivateholdings SET " . $query['after'] . "updatedDate = NOW(), updatedBy = '1703' WHERE ID = " . $item['ID'];
		}

		if (isset($bulkUpdate)) $updatedb = $this->runBulkUpdate($bulkUpdate);
		if (isset($logs))$log = $this->writeLog($logs);
	}


	public function dailyAsset(){
       $items = $this->dao->dailyAsset_query();
	   $logs = array();
		foreach ($items as $item) {

			$item["db"] = "networthalternativeassets";
			$item["tickerSymbolID"] = '';

			//POPULATING FIELDS
            $fxRate = ($item['currencyFrom'] == "USD") ? 1 : $item['exch_rate_usd'];
			$fxRate = number_format($fxRate, 5, '.', '');

            $valueofHoldingForeign = $item['valueofHoldingLocalCurrency'] * $fxRate;
            $roundedValueofHolding = $this->roundingRules($valueofHoldingForeign);

            $columns = array(
				"valueOfHolding",
				"valueofHoldingForeign", 
				"roundedValueofHolding", 
				"currencyRate"
				);

            $newValue = array(
				"valueOfHolding" => $roundedValueofHolding,
                "valueofHoldingForeign" => $valueofHoldingForeign,
                "roundedValueofHolding" => $roundedValueofHolding,
                "currencyRate" => $fxRate
            	);

			$query = $this->getQuery($item, $newValue, $columns);

			//$logs[] = "DSO>> after =>". $query['after'] . " before =>". $query['before'] . " ID=> ". $item['ID'] . " currency=> " . $item['currencyFrom'] . PHP_EOL;
			$item['before'] = $query['before'];
			$item['after'] = $query['after'];
			$logs[] = $item;
			$bulkUpdate[] = "UPDATE networthalternativeassets SET " . $query['after'] . "updatedDate = NOW(), updatedBy = '1703' WHERE ID = " . $item['ID'];
        }

		if (isset($bulkUpdate)) $updatedb = $this->runBulkUpdate($bulkUpdate);
		if (isset($logs))$log = $this->writeLog($logs);
    }


	public function calculateTotalNetWorth(){

		$db = $this->db->connectDB();

		$username = 'xxx@wealthx.com';
		$password = 'xxx@xxx';
		$loginUrl = 'https://xxx.xxx.com/login?username='.$username.'&password='.$password;
		 
		//init curl
		$ch = curl_init();
		 
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		 
		//Handle cookies for the login
		curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
		 
		//Setting CURLOPT_RETURNTRANSFER variable to 1 will force cURL
		//not to print out the results of its query.
		//Instead, it will return the results as a string return value
		//from curl_exec() instead of the usual true/false.
		//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		 
		//execute the request (the login)
		$store = curl_exec($ch);

		$db->query("SELECT DISTINCT profileID FROM factdaily_occdailylog WHERE (calculateTotalNW is null or calculateTotalNW = '')");
		$items = $db->fetch_assoc_all();

		foreach ($items as $item) {

			$id = $item['profileID'];

			//set the URL to the protected file
			curl_setopt($ch, CURLOPT_URL, 'https://xxx.xxx.com/xx/xxx/calculateTotalNetWorthExternal?profileID='.$id.'&userID=xxx');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			//execute the request
			$content = curl_exec($ch);
			 
			//the login is now done and you can continue to get the
			//protected content.

			if ($content != false) {
				echo "re-calculate done for : " . $id . PHP_EOL;
			} else {
				echo "re-calculate failed for : " . $id . PHP_EOL;
			} 

			$db->query("UPDATE factdaily_occdailylog SET calculateTotalNW = ?, calculateDate = NOW() WHERE profileID = ?", array($content, $id));
			//GRANT USAGE ON *.* TO 'wealthX'@'%' WITH MAX_CONNECTIONS_PER_HOUR 0;
		}

		$db->close();
	}




}
?>