<?php
class AppController {

	function __construct(){
		$this->db = new MysqlConnection();
    }

	/*
		Email function. Its working but not yet being implemented. 
		This is here for future use.
	*/
	public function sendEmail($emailBody){
		$body = implode("<br>", $emailBody);
		$email = new PHPMailer();
		$email->From      = 'akrishnan@wealthx.com';
		$email->FromName  = 'Factset Daily Updates';
		$email->Subject   = 'Factset Daily Updates for '. date("jS F");
		$email->Body      = $body;
		$email->AddAddress( 'akrishnan@wealthx.com' );
		return $email->Send();
	}

	/*
		The rounding Rules formula below is given by Terry and its how was done in Java backend of Reveal.
	*/
 	public function roundingRules($actualValue){
		$multiply = 0.0;
		if(!$actualValue)
			return;
			
		/**
		 * less than 100 thousand
		*/
		if ($actualValue < 100000) {
			$modeValue = $actualValue % 5000;
			if($modeValue > 0.0){
				$multiply = $actualValue - $modeValue;
			}else {
				$multiply = $actualValue;	
			}
		}
		
		/**
		 * less than 1 million
		 */
		if ($actualValue >= 100000 && $actualValue <= 999999) {
			$multiply = $this->floorValue($actualValue, 100000);
		}
		/**
		 * less than 10 million
		 */
		else if ($actualValue >= 1000000 && $actualValue <= 9999999) {
			$multiply = $this->floorValue($actualValue, 500000);
		}
		/**
		 * less than 20 million
		 */
		else if ($actualValue >= 10000000 && $actualValue <= 19999999) {
			$multiply = $this->floorValue($actualValue, 1000000);
		}
		/**
		 * less than 100 million
		 */
		else if ($actualValue >= 20000000 && $actualValue <= 99999999) {
			$multiply = $this->floorValue($actualValue, 5000000);
		}
		/**
		 * less than 1000 million
		 */
		else if ($actualValue >= 100000000 && $actualValue < 1000000000) {
			$multiply = $this->floorValue($actualValue, 10000000);
		}
		/**
		 * above than 1000000000 million
		 */
		else if ($actualValue > 1000000000) {
			$multiply = $this->floorValue($actualValue, 100000000);
		}

		//echo $multiply . PHP_EOL;
		return $multiply;
	}

	/**
	* Rounding mechanism 
	*/
	public function floorValue($actualValue, $roundedTo) {
		return floor($actualValue / $roundedTo) * ($roundedTo);
	}

	/**
	* Checking for Stocksplit
	*/
	public function stockSplit($numberofUnits, $fs_perm_sec_id, $priceDate, $dbType, $dbID){
		$splitCount = 0;
		$historyCount = 0;
		$split_factor = 0;

		$db = $this->db->connectDB();

		$db->query('SELECT P_SPLIT_FACTOR FROM fp_basic_splits WHERE fs_perm_sec_id = ? AND DATE = ? LIMIT 1', array($fs_perm_sec_id, $priceDate));
		$split = $db->fetch_assoc();
		$split_factor = $split["P_SPLIT_FACTOR"];

		if ($split_factor > 0){
			$db->query('SELECT * FROM factdaily_splithistory WHERE fs_perm_sec_id = ? AND split_date = ? AND dbType = ? AND dbID = ? ', array($fs_perm_sec_id, $priceDate, $dbType, $dbID));
			$history_returned = $db->fetch_assoc_all();
		  	$historyCount = count($history_returned);
			//$historyCount = count($history);
			if($historyCount == 0) {
				$split_factor = $split["P_SPLIT_FACTOR"];
				$numberofUnits_ori = $numberofUnits;
				$numberofUnits = $numberofUnits / $split_factor;
				$db->query('INSERT INTO factdaily_splithistory SET fs_perm_sec_id = ?, split_date = ?, from_noofunit = ?, to_noofunit = ?, datecreated = NOW(), split_factor = ?, dbType = ? , dbID = ? ', array($fs_perm_sec_id, $priceDate, $numberofUnits_ori, $numberofUnits, $split_factor, $dbType, $dbID));
			}
			//echo "stockSplit ===> $fs_perm_sec_id , $priceDate |$splitCount / $historyCount| $numberofUnits / $split_factor".PHP_EOL;
		}

		$db->close();
		return round($numberofUnits);
	}

	/**
	* Exercisable 
	**/
	public function exercisable($item){
		$actualValue = 0;
		switch ($item['exercisable']) {
				case 'true':
					if ($item['strikePrice'] >= $item['p_price']) {
						$actualValue = 0;
					} else {
						$actualValue = ($item['p_price'] * $item['numberofUnits_wsplit']) - ($item['strikePrice'] * $item['numberofUnits_wsplit']);
					}
				break;

				case 'false':
					if ((($item['numberofUnits_wsplit'] * $item['p_price']) - ($item['numberofUnits_wsplit'] * $item['strikePrice'])) <= 0) {
						$actualValue = 0;
					} else {
						$actualValue = (($item['numberofUnits_wsplit'] * $item['p_price']) - ($item['numberofUnits_wsplit'] * $item['strikePrice'])) * 0.5;
					}
				break;				
			}
		return $actualValue;
	}

	/**
	* Generates sql query 
	**/
	public function getQuery($item, $newValue, $columns){
		$comma = '';
		$query = '';
		$querylogAfter = '';
		foreach($columns as $key) {
			//echo $newValue[$key];
			if($item[$key] != $newValue[$key]) {
				$query .= $comma . $key . " = '" . $newValue[$key] . "'";
				$comma = ", ";
			}
			$querylogAfter .= $key . " = '" . $newValue[$key] . "', ";
		}

		$comma = '';
		$querylogBefore = '';
		foreach($columns as $key) {
			//echo $newValue[$key];
				$querylogBefore .= $comma . $key . " = '" . $item[$key] . "'";
				$comma = ", ";
		}

		$finalQuery = array(
			"after" => $querylogAfter,
			"before" => $querylogBefore
		);

		return $finalQuery;
	}

	/**
	* Write to factdaily_occdailylog table which is where the calculatenetworth will 
	* look for new entries and update their networth.
	**/
	public function writeLog($items){
		$db = $this->db->connectDB();
		//should update log db - not done
		foreach ($items as $item) {

			$tickerSymbolID = $item['tickerSymbolID'] ? $item['tickerSymbolID'] : '';

			$db->query("INSERT INTO factdaily_occdailylog SET 
				beforeUpdate = ?, 
				afterUpdate = ?, 
				profileID = ?, 
				recordID = ?, 
				tickerSymbolID = ?, 
				currency = ?, 
				createdDate = NOW(), 
				createdTime = NOW(), 
				runType = ?", 
				array(
					$item['before'], 
					$item['after'], 
					$item['profileID'],
					$item['ID'], 
					$tickerSymbolID, 
					$item['currency'],
					$item['db']
				)
			);
		}
		
		$db->close();
		return;
	}

	/**
	* Just for testing 
	**/
	public function writeFile($items){
		foreach ($items as $item) 
			$write = file_put_contents(
				'/opt/bitnami/apache2/htdocs/reveal_dailyupdate_script/logs/test.log', 
				"test".PHP_EOL, 
				FILE_APPEND
				);
		return;
	}

	/**
	* sql queries will send to this function to execute in bulk 
	**/
	public function runBulkUpdate($items){
		$db = $this->db->connectDB();
		//$db_occ = $this->connectDB_occ();
		foreach ($items as $item) {
			$db->query($item);
			echo $item.PHP_EOL;
		}
		$db->close();
		return;
	}

}
?>