<?php
class OCCdaily extends Wealthx {

	
	public function __construct(){
		$this->changedIDs = array(660, 661, 503292);
	}

	public function roundingRules($actualValue) {
		
		$multiply = 0.0;

		if(!$actualValue)
			return;
		/*if ($actualValue > 0 and $actualValue < 1000000) 			$roundValue = 0;
		if ($actualValue >= 1000000 and $actualValue < 10000000) 	$roundValue = round($actualValue / 500000) * 500000;
		if ($actualValue >= 10000000 and $actualValue < 20000000) 	$roundValue = round($actualValue, -6);
		if ($actualValue >= 20000000 and $actualValue < 100000000) 	$roundValue = round($actualValue / 5000000) * 5000000;
		if ($actualValue >= 100000000 and $actualValue < 1000000000)$roundValue = round($actualValue, -7);
		if ($actualValue >= 1000000000)								$roundValue = round($actualValue, -8);*/

		/**
		 * less than 100 thousand
		*/
		if ($actualValue < 100000) {
			$modeValue = $actualValue % 5000;
			if($modeValue > 0.0){
				$multiply = $actualValue - $modeValue;
			}else {
				$multiply = $actualValue;	
			}
		}

		
		/**
		 * less than 1 million
		 */
		if ($actualValue >= 100000 && $actualValue <= 999999) {
			$multiply = $this->floorValue($actualValue, 100000);
		}
		/**
		 * less than 10 million
		 */
		else if ($actualValue >= 1000000 && $actualValue <= 9999999) {
			$multiply = $this->floorValue($actualValue, 500000);
		}
		/**
		 * less than 20 million
		 */
		else if ($actualValue >= 10000000 && $actualValue <= 19999999) {
			$multiply = $this->floorValue($actualValue, 1000000);
		}
		/**
		 * less than 100 million
		 */
		else if ($actualValue >= 20000000 && $actualValue <= 99999999) {
			$multiply = $this->floorValue($actualValue, 5000000);
		}
		/**
		 * less than 1000 million
		 */
		else if ($actualValue >= 100000000 && $actualValue < 1000000000) {
			$multiply = $this->floorValue($actualValue, 10000000);
		}
		/**
		 * above than 1000000000 million
		 */
		else if ($actualValue > 1000000000) {
			$multiply = $this->floorValue($actualValue, 100000000);
		}

		//echo $multiply . PHP_EOL;
		return $multiply;
	}


	private function floorValue($actualValue, $roundedTo) {
		
		return floor($actualValue / $roundedTo) * ($roundedTo);

	}


	public function stockSplit($numberofUnits, $fs_perm_sec_id, $priceDate, $dbType, $dbID) {
		
		$splitCount = 0;
		$historyCount = 0;
		$split_factor = 0;

		$db = $this->connectDB();

		$db->query('SELECT P_SPLIT_FACTOR FROM fp_basic_splits WHERE fs_perm_sec_id = ? AND DATE = ? LIMIT 1', array($fs_perm_sec_id, $priceDate));
		$split = $db->fetch_assoc();
		$split_factor = $split["P_SPLIT_FACTOR"];

		if ($split_factor > 0){

			$db->query('SELECT * FROM factdaily_splithistory WHERE fs_perm_sec_id = ? AND split_date = ? AND dbType = ? AND dbID = ? ', array($fs_perm_sec_id, $priceDate, $dbType, $dbID));
			$history_returned = $db->fetch_assoc_all();
		  	$historyCount = count($history_returned);

			//$historyCount = count($history);
			if($historyCount == 0) {
				$split_factor = $split["P_SPLIT_FACTOR"];
				$numberofUnits_ori = $numberofUnits;
				$numberofUnits = $numberofUnits / $split_factor;
				$db->query('INSERT INTO factdaily_splithistory SET fs_perm_sec_id = ?, split_date = ?, from_noofunit = ?, to_noofunit = ?, datecreated = NOW(), split_factor = ?, dbType = ? , dbID = ? ', array($fs_perm_sec_id, $priceDate, $numberofUnits_ori, $numberofUnits, $split_factor, $dbType, $dbID));
			}

			//echo "stockSplit ===> $fs_perm_sec_id , $priceDate |$splitCount / $historyCount| $numberofUnits / $split_factor".PHP_EOL;
		}
		
		return round($numberofUnits);
	}



	public function dailyStockOption () {
		$db = $this->connectDB();
		$items = array();

		$db->query("SELECT manualSharePrice, profileID, valueOfHolding, actualValueofHolding, roundedValueofHolding, ID, numberofUnits, strikePrice, currency, tickerSymbolID, exercisable, fxRate, marketPrice, priceDate FROM networthpublicholdingsstockoptions WHERE (tickerSymbolID != '' or tickerSymbolID is not null)");
		$publicCompanies = $db->fetch_assoc_all();   
		$count = count($publicCompanies);

		foreach ($publicCompanies as $pc) {


			if ($pc['manualSharePrice'] > 0)
				continue;

			$id = $pc['ID'];
			$numberofUnits = $pc['numberofUnits'];
			$strikePrice = $pc['strikePrice'];
			$currency = $pc['currency'];
			$profileID = $pc['profileID'];
			$tickerSymbolID = $pc['tickerSymbolID'];
			$exercisable = $pc['exercisable'];
			$actualValue = 0;
			$roundValue = 0;

			//CHECK IF LOG EXIST, ELSE SKIP THIS RECORD
			$db->query("SELECT * FROM factdaily_occdailylog WHERE recordID = ? AND createdDate = CURDATE() AND runType = 'dailyStockOption'", array($id));
			$log_returned = $db->fetch_assoc_all();
		  	$logCount = count($log_returned);

		  	if ($logCount > 0) 
				continue;


			if ($numberofUnits < 1) 
				continue;


			$db->query('SELECT fs_perm_sec_id FROM fact_security_ticker WHERE ticker_exchange = ? LIMIT 1', array($tickerSymbolID));
			$ticker = $db->fetch_assoc();
			$fs_perm_sec_id = $ticker['fs_perm_sec_id'];

			$db->query('SELECT p_price, DATE FROM fp_basic_bd WHERE fs_perm_sec_id = ? ORDER BY DATE DESC LIMIT 1', array($fs_perm_sec_id));
			$price = $db->fetch_assoc();
			$marketPrice = $price['p_price'];
			$priceDate = $price['DATE'];

			if (!$priceDate)
				continue;


			if ($currency == "USD") {
				$fxRate = 1;
			} else {				
				$db->query('SELECT exch_rate_usd FROM fx_rates_usd WHERE iso_currency = ? ORDER BY DATE DESC LIMIT 1', array($currency));
				$ticker = $db->fetch_assoc();
				$fxRate = $ticker['exch_rate_usd'];
			}

			if (!$fxRate)
				continue;
			

			$fxRate = number_format($fxRate, 5, '.', '');

			//check for stock split and update numberofUnits
			$numberofUnits_wsplit = $this->stockSplit($numberofUnits, $fs_perm_sec_id, $priceDate, "networthpublicholdingsstockoptions", $id);

			switch ($exercisable) {
				case 'true':
					if ($strikePrice >= $marketPrice) {
						$actualValue = 0;
					} else {
						$actualValue = ($marketPrice * $numberofUnits_wsplit) - ($strikePrice * $numberofUnits_wsplit);
					}
				break;

				case 'false':
					if ((($numberofUnits_wsplit * $marketPrice) - ($numberofUnits_wsplit * $strikePrice)) <= 0) {
						$actualValue = 0;
					} else {
						$actualValue = (($numberofUnits_wsplit * $marketPrice) - ($numberofUnits_wsplit * $strikePrice)) * 0.5;
					}
				break;				
			}

			$actualValue = $actualValue * $fxRate;

			$roundValue = $this->roundingRules($actualValue);
			

			$roundValue = floor($roundValue);
			$actualValue = number_format($actualValue, 2, '.', '');



			//IF ROUNDVALUE IS ZERO THEN ASSETSWATCH SHOULD BE FALSE
			if ($roundValue > 0) {

				$columns = array("valueOfHolding", "actualValueofHolding", "roundedValueofHolding", "numberofUnits", "fxRate", "marketPrice", "priceDate");
				$newValue = array(
						"valueOfHolding" => $roundValue,
						"actualValueofHolding" => $actualValue,
						"roundedValueofHolding" => $roundValue,
						"numberofUnits" => $numberofUnits_wsplit,
						"fxRate" => $fxRate,
						"marketPrice" => $marketPrice,
						"priceDate" => $priceDate
					);

			} else {

				$includeInAssetSwatch = 'false';	
				$columns = array("valueOfHolding", "actualValueofHolding", "roundedValueofHolding", "numberofUnits", "fxRate", "marketPrice", "priceDate", "includeInAssetSwatch");
				$newValue = array(
						"valueOfHolding" => $roundValue,
						"actualValueofHolding" => $actualValue,
						"roundedValueofHolding" => $roundValue,
						"numberofUnits" => $numberofUnits_wsplit,
						"fxRate" => $fxRate,
						"marketPrice" => $marketPrice,
						"priceDate" => $priceDate,
						"includeInAssetSwatch" => $includeInAssetSwatch
					);
			}


			



			$comma = '';
			$query = '';
			$querylogAfter = '';
			foreach($columns as $key) {
				//echo $newValue[$key];
			    if($pc[$key] != $newValue[$key]) {
			        $query .= $comma . $key . " = '" . $newValue[$key] . "'";
			        $comma = ", ";
			    }
			    $querylogAfter .= $key . " = '" . $newValue[$key] . "', ";
			}

			$comma = '';
			$querylogBefore = '';
			foreach($columns as $key) {
				//echo $newValue[$key];
			        $querylogBefore .= $comma . $key . " = '" . $pc[$key] . "'";
			        $comma = ", ";
			}
			
			echo "dailyStockOption>>>> ". $query . " ==> ". $pc['ID'] . PHP_EOL;
			//$db->query('UPDATE networthpublicholdingsstockoptions fx')
			//echo "$id | $fs_perm_sec_id | $numberofUnits | $priceDate | $marketPrice / $strikePrice | $currency $actualValue / $roundValue" . PHP_EOL;
			//echo "dailyStockOption ===> $tickerSymbolID , $fs_perm_sec_id | noofunit : $numberofUnits / $numberofUnits_wsplit | currency : $currency | Price : $priceDate, $strikePrice / $marketPrice | fxRate : $fxRate | Actual Value : $actualValue | Rounded Value : $roundValue" . PHP_EOL;
			//$db->query('UPDATE networthpublicholdingsstockoptions SET fxRate = ?, marketPrice = ?, priceDate = ?, actualValueofHolding = ?, roundedValueofHolding = ?, valueofHolding = ?, updatedDate = NOW(), updaterID = \'1703\' WHERE ID = ?', array($fxRate, $marketPrice, $priceDate, $actualValue, $roundValue, $roundValue, $id));

			if ($query != "") {
				$db->query("INSERT INTO factdaily_occdailylog SET beforeUpdate = ?, afterUpdate = ?, profileID = ?, recordID = ?, tickerSymbolID = ?, currency = ?, createdDate = NOW(), createdTime = NOW(), runType = 'dailyStockOption'", array($querylogBefore, $querylogAfter, $profileID, $id, $tickerSymbolID, $currency));
				$db->query("UPDATE networthpublicholdingsstockoptions SET " . $query . " , updatedDate = NOW(), updaterID = '1703' WHERE ID = ?", array($id));
			}

		}
	}



	public function dailyCommonStock () {
		$db = $this->connectDB();
		$items = array();

		$db->query("SELECT manualSharePrice, profileID, valueOfHolding, roundedValueofHolding, actualValueofHolding, ID, numberofUnits, currency, tickerSymbolID, fxRate, marketPrice, priceDate FROM networthpublicholdingscommonstock WHERE (tickerSymbolID != '' or tickerSymbolID is not null) ORDER BY profileID DESC");
		$publicCompanies = $db->fetch_assoc_all();

		foreach ($publicCompanies as $pc) {

			if ($pc['manualSharePrice'] > 0)
				continue;

			$id = $pc['ID'];
			$numberofUnits = $pc['numberofUnits'];
			$currency = $pc['currency'];
			$tickerSymbolID = $pc['tickerSymbolID'];
			$profileID = $pc['profileID'];
			$actualValue = 0;
			$roundValue = 0;

			$pc['roundedValueofHolding'];


			//CHECK IF LOG EXIST, ELSE SKIP THIS RECORD
			$db->query("SELECT * FROM factdaily_occdailylog WHERE recordID = ? AND createdDate = CURDATE() AND runType = 'dailyCommonStock'", array($id));
			$log_returned = $db->fetch_assoc_all();
		  	$logCount = count($log_returned);

		  	

		  	if ($logCount > 0) 
				continue;


			if ($numberofUnits < 1)
				continue;

			$db->query('SELECT fs_perm_sec_id FROM fact_security_ticker WHERE ticker_exchange = ? LIMIT 1', array($tickerSymbolID));
			$ticker = $db->fetch_assoc();
			$fs_perm_sec_id = $ticker['fs_perm_sec_id'];

			$db->query('SELECT p_price, DATE FROM fp_basic_bd WHERE fs_perm_sec_id = ? ORDER BY DATE DESC LIMIT 1', array($fs_perm_sec_id));
			$price = $db->fetch_assoc();
			$marketPrice = $price['p_price'];
			$priceDate = $price['DATE'];

			if (!$priceDate)
				continue;


			if ($currency == "USD") {
				$fxRate = 1;
			} else {

				$db->query('SELECT exch_rate_usd FROM fx_rates_usd WHERE iso_currency = ? ORDER BY DATE DESC LIMIT 1', array($currency));
				$fx = $db->fetch_assoc();
				$fxRate = $fx['exch_rate_usd'];

			}

			if (!$fxRate)
				continue;
			

			$fxRate = number_format($fxRate, 5, '.', '');

			//check for stock split and update numberofUnits
			$numberofUnits_wsplit = $this->stockSplit($numberofUnits, $fs_perm_sec_id, $priceDate, "networthpublicholdingscommonstock", $id);


			$actualValue = ($marketPrice * $numberofUnits_wsplit) * $fxRate;

			
			$roundValue = $this->roundingRules($actualValue);


			$roundValue = floor($roundValue);
			$actualValue = number_format($actualValue, 2, '.', '');
			//to be added stock split checking then update number of units/opt/bitnami/php/bin



			//IF ROUNDVALUE IS ZERO THEN ASSETSWATCH SHOULD BE FALSE
			if ($roundValue > 0) {

				$columns = array("valueOfHolding", "actualValueofHolding", "roundedValueofHolding", "numberofUnits", "fxRate", "marketPrice", "priceDate");
				$newValue = array(
						"valueOfHolding" => $roundValue,
						"actualValueofHolding" => $actualValue,
						"roundedValueofHolding" => $roundValue,
						"numberofUnits" => $numberofUnits_wsplit,
						"fxRate" => $fxRate,
						"marketPrice" => $marketPrice,
						"priceDate" => $priceDate
					);

			} else {

				$includeInAssetSwatch = 'false';
				$columns = array("valueOfHolding", "actualValueofHolding", "roundedValueofHolding", "numberofUnits", "fxRate", "marketPrice", "priceDate", "includeInAssetSwatch");
				$newValue = array(
						"valueOfHolding" => $roundValue,
						"actualValueofHolding" => $actualValue,
						"roundedValueofHolding" => $roundValue,
						"numberofUnits" => $numberofUnits_wsplit,
						"fxRate" => $fxRate,
						"marketPrice" => $marketPrice,
						"priceDate" => $priceDate,
						"includeInAssetSwatch" => $includeInAssetSwatch
					);	
			}



			$comma = '';
			$query = '';
			$querylogAfter = '';
			foreach($columns as $key) {
				//echo $newValue[$key];
			    if($pc[$key] != $newValue[$key]) {
			        $query .= $comma . $key . " = '" . $newValue[$key] . "'";
			        $comma = ", ";
			    }
			    $querylogAfter .= $key . " = '" . $newValue[$key] . "', ";
			}

			$comma = '';
			$querylogBefore = '';
			foreach($columns as $key) {
				//echo $newValue[$key];
			        $querylogBefore .= $comma . $key . " = '" . $pc[$key] . "'";
			        $comma = ", ";
			}

			echo "dailyCommonStock>>>> ". $query . " ==> ". $pc['ID'] . PHP_EOL;
			//$db->query('UPDATE networthpublicholdingsstockoptions fx')
			//echo "$id | $fs_perm_sec_id | $numberofUnits | $priceDate | $marketPrice | $currency $actualValue / $roundValue" . PHP_EOL;
			//echo "dailyCommonStock ===> $tickerSymbolID | noofunit : $numberofUnits / $numberofUnits_wsplit | currency : $currency | Price : $priceDate, $marketPrice | fxRate : $fxRate | Actual Value : $actualValue | Rounded Value : $roundValue" . PHP_EOL;
			//$db->query('UPDATE networthpublicholdingscommonstock SET fxRate = ?, marketPrice = ?, priceDate = ?, actualValueofHolding = ?, roundedValueofHolding = ?, valueofHolding = ? WHERE ID = ?', array($fxRate, $marketPrice, $priceDate, $actualValue, $roundValue, $roundValue, $id));

			if ($query != "") {
				$db->query("INSERT INTO factdaily_occdailylog SET beforeUpdate = ?, afterUpdate = ?, profileID = ?, recordID = ?, tickerSymbolID = ?, currency = ?, createdDate = NOW(), createdTime = NOW(), runType = 'dailyCommonStock'", array($querylogBefore, $querylogAfter, $profileID, $id, $tickerSymbolID, $currency));
				$db->query("UPDATE networthpublicholdingscommonstock SET " . $query . " , updatedDate = NOW(), updaterID = '1703' WHERE ID = ?", array($id));
			}

		}
	} 

	public function weeklyPublicDividends () {
		$db = $this->connectDB();
		$items = array();

		//RPdatabase.publicdividentinfo
		//dividentID, profileID, yearValue, companyID, tickerID, numberofUnits, baseCurrency, foreignCurrency, fxRate, dividents, TotalValue, createrID, createdDate, updaterID, updatedDate
		$db->query('SELECT profileID, dividentID, yearValue, numberofUnits, baseCurrency, foreignCurrency, dividents AS totalDividends, TotalValue AS totalValue, fxRate, tickerID, updatedDate FROM occpublicdividend WHERE tickerID is not null');
		$publicCompanies = $db->fetch_assoc_all();

		foreach ($publicCompanies as $pc) {
			$id = $pc['dividentID'];
			$profileID = $pc['profileID'];
			$numberofUnits = $pc['numberofUnits'];
			$baseCurrency = $pc['baseCurrency'];
			$foreignCurrency = $pc['foreignCurrency'];
			$tickerID = $pc['tickerID'];
			$totalValue = $pc['totalValue'];
			$yearValue = $pc['yearValue'];
			$dividends = $pc['totalDividends'];
			$updatedDate = date("Y-m-d", strtotime($pc['updatedDate']));
			$totalDividends = 0;
			$fxRate = 0;

			//CHECK IF LOG EXIST, ELSE SKIP THIS RECORD
			$db->query("SELECT * FROM factdaily_occdailylog WHERE recordID = ? AND createdDate = CURDATE() AND runType = 'weeklyPublicDividends'", array($id));
			$log_returned = $db->fetch_assoc_all();
		  	$logCount = count($log_returned);

		  	if ($logCount > 0) 
				continue;


			if ($numberofUnits < 1)
				continue;

			$db->query('SELECT fs_perm_sec_id FROM fact_security_ticker WHERE ticker_exchange = ? LIMIT 1', array($tickerID));
			$ticker = $db->fetch_assoc();
			$fs_perm_sec_id = $ticker['fs_perm_sec_id'];

			$db->query('SELECT p_price, currency, DATE FROM fp_basic_bd WHERE fs_perm_sec_id = ? ORDER BY DATE DESC LIMIT 1', array($fs_perm_sec_id));
			$prices = $db->fetch_assoc();
			$marketPrice = $prices['p_price'];
			$tradeCurrency = $prices['currency'];
			$priceDate = $prices['DATE'];

			$db->query('SELECT exch_rate_usd FROM fx_rates_usd WHERE iso_currency = ? ORDER BY DATE DESC LIMIT 1', array($baseCurrency));
			$fx = $db->fetch_assoc();

			if ($baseCurrency == "USD"){
				$baseCurrencyfxRate = 1;
			} else {
				$baseCurrencyfxRate = $fx['exch_rate_usd'];
			}
			

			$db->query('SELECT exch_rate_usd FROM fx_rates_usd WHERE iso_currency = ? ORDER BY DATE DESC LIMIT 1', array($foreignCurrency));
			$fx = $db->fetch_assoc();

			if ($foreignCurrency == "USD"){
				$foreignCurrencyfxRate = 1;
			} else {
				$foreignCurrencyfxRate = $fx['exch_rate_usd'];
			}
			

			$db->query('SELECT exch_rate_usd FROM fx_rates_usd WHERE iso_currency = ? ORDER BY DATE DESC LIMIT 1', array($tradeCurrency));
			$fx = $db->fetch_assoc();
			$tradeCurrencyfxRate = $fx['exch_rate_usd'];

			$db->query('SELECT sum(P_DIVS_PD) as totalDividends FROM fp_basic_dividends where fs_perm_sec_id = ? and year(DATE) = ?', array($fs_perm_sec_id, $yearValue));
			$div = $db->fetch_assoc();
			$totalDividends = $div['totalDividends'];

			$db->query('SELECT P_SPLIT_FACTOR, DATE FROM fp_basic_splits where fs_perm_sec_id = ? and DATE > ? ORDER BY DATE DESC LIMIT 1', array($fs_perm_sec_id, $updatedDate));
			$split = $db->fetch_assoc();
			$splitFactor = $split['P_SPLIT_FACTOR'];
			$splitDate = $split['DATE'];

			//if ($splitFactor > 0 and $yearValue == date("Y", strtotime($splitDate))) {
				//$numberofUnits = $numberofUnits / $splitFactor;
				//$db->query('UPDATE publicdividentinfo SET numberofUnits = ?, updatedDate = NOW() WHERE dividentID = ?', array($numberofUnits, $id));
			//}

			$numberofUnits_wsplit = $this->stockSplit($numberofUnits, $fs_perm_sec_id, $priceDate, "occpublicdividend", $id);


			//Only run the below codes if this record is the current year.
			if ($yearValue != date("Y")) 
				continue;

			$fxRate = number_format(($tradeCurrencyfxRate / $baseCurrencyfxRate), 5, '.', ''); 
			$totalDividends = $totalDividends * $fxRate;
			$totalValue = number_format(($numberofUnits_wsplit * $totalDividends), 2, '.', '');
			//$db->query('UPDATE publicdividentinfo SET foreignCurrency = ?, updatedDate = NOW() WHERE dividentID = ?', array($tradeCurrency, $id));


			$totalValue = number_format($totalValue, 2, '.', '');


			$columns = array("totalValue", "numberofUnits", "fxRate");
			//"actualValueofHolding", "roundedValueofHolding", "numberofUnits", "fxRate", "marketPrice", "priceDate"
			$newValue = array(
					"totalValue" => $totalValue,
					"numberofUnits" => $numberofUnits_wsplit,
					"fxRate" => $fxRate
				);


			$comma = '';
			$query = '';
			$querylogAfter = '';
			foreach($columns as $key) {
				//echo $newValue[$key];
			    if($pc[$key] != $newValue[$key]) {
			        $query .= $comma . $key . " = '" . $newValue[$key] . "'";
			        $comma = ", ";
			    }
			    $querylogAfter .= $key . " = '" . $newValue[$key] . "', ";
			}

			$comma = '';
			$querylogBefore = '';
			foreach($columns as $key) {
				//echo $newValue[$key];
			        $querylogBefore .= $comma . $key . " = '" . $pc[$key] . "'";
			        $comma = ", ";
			}

			echo "weeklyPublicDividends>>>> ". $query . " ==> ID: ". $pc['dividentID'] . " ==> Year:". $yearValue . PHP_EOL;
			//echo "$id | $fs_perm_sec_id | $numberofUnits | $priceDate | $marketPrice | $foreignCurrencyfxRate / $baseCurrencyfxRate ($fxRate) | $dividends / $totalDividends | $updatedDate / $splitDate | $splitFactor | $totalValue" . PHP_EOL;
			//echo "weeklyPublicDividends ===> $tickerID | Price : $priceDate, $marketPrice | numberofUnits : $numberofUnits | totalDividends : $totalDividends | splitFactor : $splitFactor | totalValue : $totalValue" . PHP_EOL;
			//$actualValue = ($marketPrice * $numberofUnits) * $fxRate;
			//$roundValue = $this->roundingRules($actualValue);

			$currency = $baseCurrency . "/" . $foreignCurrency;

			if ($query != "") {
				$db->query("INSERT INTO factdaily_occdailylog SET beforeUpdate = ?, afterUpdate = ?, profileID = ?, recordID = ?, tickerSymbolID = ?, currency = ?, createdDate = NOW(), createdTime = NOW(), runType = 'weeklyPublicDividends'", array($querylogBefore, $querylogAfter, $profileID, $id, $tickerID, $currency));
				$db->query("UPDATE occpublicdividend SET " . $query . " , updatedDate = NOW(), updaterID = '1703' WHERE dividentID = ?", array($id));
			}
		}

	}


	public function weeklyPrivateValuation () {
		$db = $this->connectDB();
		$items = array();

		//ID, profileID, sharesNo, percentageHoldings, calculatedCompSharePrice, calcAvgMarketCap, includeInAssetSwatch, valueOfHolding, lastUpdated, assetRemarks, editorNotes, version, companyID, typeOf, 
		//Currency, currencyValue, assetValue, assetYear, debtRatio, revenue, revenueYear, startYear, sector, industry, priceToSaleRatio, acctualValueofHoldingLocal, actualValueofHoldingUSD, roundedValueofHoldingUSD, 
		//assumptions, ownership, createdBy, createdDate, updatedBy, updatedDate, ID, id
		$db->query('SELECT *, currencyValue as fxRate FROM networthprivateholdings where industry is not null and ownership is not null and revenue is not null ORDER BY profileID DESC');
		$privateCompanies = $db->fetch_assoc_all();

		foreach ($privateCompanies as $pc) {
			$id = $pc['ID'];
			$profileID = $pc['profileID'];
			$companyID = $pc['companyID'];
			$ownership = $pc['ownership'];
			$revenue = $pc['revenue'];
			$Currency = $pc['Currency'];
			$industry = $pc['industry'];
			$psRatioManual = $pc['psRatioManual'];
			$priceToSaleRatio = 0;

			//CHECK IF LOG EXIST, ELSE SKIP THIS RECORD
			$db->query("SELECT * FROM factdaily_occdailylog WHERE recordID = ? AND createdDate = CURDATE() AND runType = 'weeklyPrivateValuation'", array($id));
			$log_returned = $db->fetch_assoc_all();
		  	$logCount = count($log_returned);

		  	if ($logCount > 0) 
				continue;


			//Manual override
			if ($psRatioManual > 0)
				continue;


			if ($Currency == "USD") {
				$fxRate = 1;
			} else {				
				$db->query('SELECT exch_rate_usd FROM fx_rates_usd WHERE iso_currency = ? ORDER BY DATE DESC LIMIT 1', array($Currency));
				$fx = $db->fetch_assoc();
				$fxRate = $fx['exch_rate_usd'];

				if (!$fxRate)
				continue;
			}	
			
			
			$fxRate = number_format($fxRate, 5, '.', '');

			$db->query('SELECT P_S FROM weax_industry_ratio where Industry_Num = ?', array($industry));
			$ind = $db->fetch_assoc();
			$priceToSaleRatio = $ind['P_S'];

			if (!$priceToSaleRatio or $priceToSaleRatio == 0)
				continue;

			$priceToSaleRatio = number_format($priceToSaleRatio, 5, '.', '');

			if ($ownership < 1)
				continue;

			$mil = 1000000;
			$revenue = $revenue * $mil;

			//$new_width = ($percentage / 100) * $totalWidth;

			$acctualValueofHoldingLocal = number_format(round(((($ownership / 100) * $priceToSaleRatio) * $revenue)), 5, '.', '');

			//$db->query('UPDATE publicdividentinfo SET numberofUnits = ?, updatedDate = NOW() WHERE dividentID = ?', array($numberofUnits, $id));				

			$actualValueofHoldingUSD = number_format(round(($acctualValueofHoldingLocal * $fxRate)), 5, '.', '');

			$roundedValueofHoldingUSD = number_format(round(($this->roundingRules($actualValueofHoldingUSD))), 5, '.', '');


			$columns = array("ownership", "priceToSaleRatio", "acctualValueofHoldingLocal", "actualValueofHoldingUSD", "roundedValueofHoldingUSD", "valueOfHolding");
			//"actualValueofHolding", "roundedValueofHolding", "numberofUnits", "fxRate", "marketPrice", "priceDate"
			$newValue = array(
					"ownership" => $ownership,
					"priceToSaleRatio" => $priceToSaleRatio,
					"acctualValueofHoldingLocal" => $acctualValueofHoldingLocal,
					"actualValueofHoldingUSD" => $actualValueofHoldingUSD,
					"roundedValueofHoldingUSD" => $roundedValueofHoldingUSD,
					"valueOfHolding" => $roundedValueofHoldingUSD

				);


			$comma = '';
			$query = '';
			$querylogAfter = '';
			foreach($columns as $key) {
				//echo $newValue[$key];
			    if($pc[$key] != $newValue[$key]) {
			        $query .= $comma . $key . " = '" . $newValue[$key] . "'";
			        $comma = ", ";
			    }
			    $querylogAfter .= $key . " = '" . $newValue[$key] . "', ";
			}

			$comma = '';
			$querylogBefore = '';
			foreach($columns as $key) {
				//echo $newValue[$key];
			        $querylogBefore .= $comma . $key . " = '" . $pc[$key] . "'";
			        $comma = ", ";
			}

			echo "weeklyPrivateValuation>>>> ".  $pc['profileID'] . " --- " . $query . PHP_EOL;
			//echo "weeklyPrivateValuation ===> $id | $ownership | $priceToSaleRatio | $revenue / $revenue_new / $revenue_fxrate | $Currency/$fxRate " . PHP_EOL;
			//$actualValue = ($marketPrice * $numberofUnits) * $fxRate;
			//$roundValue = $this->roundingRules($actualValue);

			if ($query != "") {
				$db->query("INSERT INTO factdaily_occdailylog SET beforeUpdate = ?, afterUpdate = ?, profileID = ?, recordID = ?, tickerSymbolID = ?, currency = ?, createdDate = NOW(), createdTime = NOW(), runType = 'weeklyPrivateValuation'", array($querylogBefore, $querylogAfter, $profileID, $id, $companyID, $Currency));
				$db->query("UPDATE networthprivateholdings SET " . $query . " , updatedDate = NOW(), updatedBy = '1703' WHERE ID = ?", array($id));
			}

		}

	}


	
	public function weeklyPrivateDividends () {
		$db = $this->connectDB();
		$items = array();

		//ID, profileID, estimatedNetWorthLower, estimatedNetWorthUpper, estimatedLiquidAssetLower, estimatedLiquidAssetUpper, editorNotes, version, 
		//estimatedNetWorthLowerActual, estimatedNetWorthUpperActual, estimatedLiquidAssetLowerActual, estimatedLiquidAssetUpperActual, baseCurrency, 
		//businessCountryId, ID, id

		//dividentTaxID, yearValue, dividenttaxRate, countryID, createrID, createdDate, updaterID, updatedDate, dividentTaxID, id

		//dividentID, valuationID, profileID, year, revenue, profitMargin, dividendPayoutMargin, ownership, growthRate, earnings, profit, 
		//baseCurrency, foreignCurrency, fXRate, payoutLocal, payoutForeign, companyID, createdBy, createdDate, updatedBy, updatedDate, 
		//dividentID, id
		//revenue, profitMargin, dividendPayoutMargin, ownership, growthRate, earnings, profit, baseCurrency, foreignCurrency, fXRate, payoutLocal, 
		//payoutForeign,
		
		$db->query('SELECT * FROM oocprivatedividend where growthRate is not null and ownership is not null and revenue is not null');
		$privateCompanies = $db->fetch_assoc_all();

		foreach ($privateCompanies as $pc) {
			$id = $pc['dividentID'];
			$year = $pc['year'];
			$ownership = $pc['ownership'];
			$revenue = $pc['revenue'];
			$profitMargin = $pc['profitMargin'];
			$dividendPayoutMargin = $pc['dividendPayoutMargin'];
			$ownership = $pc['ownership'];
			$growthRate = $pc['growthRate'];
			$baseCurrency = $pc['baseCurrency'];
			$foreignCurrency = $pc['foreignCurrency'];
			$profileID = $pc['profileID'];

			$discountRate = 0;
			$discountFactor = 0;
			$discount = 0;
			$fXRate = 0;
			$payoutLocal = 0;
			$payoutForeign = 0;

			$db->query('SELECT exch_rate_usd FROM fx_rates_usd WHERE iso_currency = ? ORDER BY DATE DESC LIMIT 1', array($baseCurrency));
			$fx = $db->fetch_assoc();
			$baseCurrencyfxRate = $fx['exch_rate_usd'];

			$db->query('SELECT exch_rate_usd FROM fx_rates_usd WHERE iso_currency = ? ORDER BY DATE DESC LIMIT 1', array($foreignCurrency));
			$fx = $db->fetch_assoc();
			$foreignCurrencyfxRate = $fx['exch_rate_usd'];

			$db->query('SELECT businessCountryId FROM networthsummary WHERE profileID = ? LIMIT 1', array($profileID));
			$ns = $db->fetch_assoc();
			$countryID = $ns['businessCountryId'];

			$db->query('SELECT dividenttaxRate FROM occdividenttaxrates WHERE yearValue = ? AND countryID = ? LIMIT 1', array($year, $countryID));
			$tr = $db->fetch_assoc();
			$dividenttaxRate = $tr['dividenttaxRate'];
			
			$fxRate = number_format(($foreignCurrencyfxRate / $baseCurrencyfxRate), 5, '.', ''); 

			if ($ownership > 0 and $growthRate > 0) {
				//$new_width = ($percentage / 100) * $totalWidth;
				$earnings = number_format((($profitMargin / 100) * $revenue), 2, '.', '');
				$profit = number_format(((($dividendPayoutMargin / 100) * $earnings) * ($ownership / 100)), 2, '.', '');
				//$db->query('UPDATE publicdividentinfo SET numberofUnits = ?, updatedDate = NOW() WHERE dividentID = ?', array($numberofUnits, $id));	

				$earnings_usd = number_format(($earnings * $fxRate), 2, '.', '');			
			}

			if ($year < 1985) {
				$spendRate = (20/100); // hard coded to 20%
				$discountRate = 1.035;
				$discountFactor = (1985 - $year);
				$discount = pow($discountRate, $discountFactor);
				$payoutLocal = $profit * ($dividenttaxRate * $spendRate) * $discount;
				$payoutForeign = $payoutLocal * $fxRate;
			} else {
				$payoutLocal = $profit * 100000; // Multiply to get in actual millions figure
				$payoutForeign = $payoutLocal * $fxRate;
			}

			/* 
			FROM TERRY'S REQUIREMENT
			If Year <1985
				If ( Year < Start Year )
					Then 0
				Else
					(Profit (Pre-Tax)* [ (1-Effective Dividend Tax Rate)*(1-Spend_Rate)] ) *(Discount Rate ^ Discount Factor))
			Else
				Profit (Pre-Tax) * 100,000 [Multiply to get in actual millions figure]
			*/

			echo "$id | $year | $ownership | $growthRate | $earnings / $profit / $earnings_usd | Country: $countryID | discount : $discountRate / $discountFactor = $discount | payout : $payoutLocal / $payoutForeign / $fxRate ($baseCurrency/$foreignCurrency)" . PHP_EOL;

			//$actualValue = ($marketPrice * $numberofUnits) * $fxRate;
			//$roundValue = $this->roundingRules($actualValue);
		}

	}

	public function calculateTotalNetWorth(){

		$db = $this->connectDB();

		$username = 'system.user@wealthx.com';
		$password = 'system.user@321';
		$loginUrl = 'https://cornerstone.wealthx.com/occ/login?username='.$username.'&password='.$password;
		 
		//init curl
		$ch = curl_init();
		 
		//Set the URL to work with
		curl_setopt($ch, CURLOPT_URL, $loginUrl);
		 
		//Handle cookies for the login
		curl_setopt($ch, CURLOPT_COOKIEJAR, '/home/akilon/factsetdaily/cookie.txt');
		 
		//Setting CURLOPT_RETURNTRANSFER variable to 1 will force cURL
		//not to print out the results of its query.
		//Instead, it will return the results as a string return value
		//from curl_exec() instead of the usual true/false.
		//curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		 
		//execute the request (the login)
		$store = curl_exec($ch);

		$db->query("SELECT DISTINCT profileID FROM factdaily_occdailylog WHERE calculateTotalNW is null");
		$privateCompanies = $db->fetch_assoc_all();

		foreach ($privateCompanies as $pc) {

			$id = $pc['profileID'];

			//set the URL to the protected file
			curl_setopt($ch, CURLOPT_URL, 'https://cornerstone.wealthx.com/occ/occ/networth/calculateTotalNetWorthExternal?profileID='.$id.'&userID=1703');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			//execute the request
			$content = curl_exec($ch);
			 
			//the login is now done and you can continue to get the
			//protected content.

			if ($content != false) {
				echo "re-calculate done for : " . $id . PHP_EOL;
			} else {
				echo "re-calculate failed for : " . $id . PHP_EOL;
			} 

			$db->query("UPDATE factdaily_occdailylog SET calculateTotalNW = ?, calculateDate = NOW() WHERE profileID = ?", array($content, $id));
			//GRANT USAGE ON *.* TO 'wealthX'@'%' WITH MAX_CONNECTIONS_PER_HOUR 0;
		}
		
	}


	
}
?>


























